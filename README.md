# OpenML dataset: Stars-from-Gaia-DR2-and-RAVE-DR5

https://www.openml.org/d/43669

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The RAVE dataset along with Gaia DR1 was used by Zackrisson et al. (2018), a paper on Dysonian SETI.
Gaia is a mission of the European Space Agency (ESA) that aims to accurately measure the position, distance and magnitude of over a billion stars. RAVE is a radial velocity dataset. RAVE also provides spectrophotometric parallax data, as well as cross-identification of stars with a number of other datasets, including Gaia DR2.
Content
This dataset is a combination of RAVE DR5 and Gaia DR2 sources. The data is obtained using the query tool of the RAVE project. The SQL query follows:
SELECT G.source_id,G.parallax,G.parallax_error,G.ra,G.dec,G.phot_g_mean_mag,G.phot_bp_mean_mag,G.phot_rp_mean_mag,G.l,G.b,G.pmra,G.pmdec, R.HRV AS r_hrv,R.Met_K AS r_metallicity,R.Algo_Conv_K AS r_quality,R.Mg AS r_mg,R.Si AS r_si,R.Ti AS r_ti,R.Fe AS r_fe,R.Ni AS r_ni,R.distance r_distance,R.parallax r_parallax,R.Jmag_2MASS r_jmag_2mass,R.Hmag_2MASS r_hmag_2mass,R.Kmag_2MASS r_kmag_2mass, R.RAdeg r_ra,R.DEdeg r_de, W1mag_ALLWISE r_w1mag_allwise, W2mag_ALLWISE r_w2mag_allwise, W3mag_ALLWISE r_w3mag_allwise, W4mag_ALLWISE r_w4mag_allwise, BTmag_TYCHO2 r_btmag_tycho2, VTmag_TYCHO2 r_vtmag_tycho2, Bmag_APASSDR9 r_bmag_apassdr9, Vmag_APASSDR9 r_vmag_apassdr9, rpmag_APASSDR9 r_rpmag_apassdr9, ipmag_APASSDR9 r_ipmag_apassdr9, Imag_DENIS r_imag_denis, Jmag_DENIS r_jmag_denis, Kmag_DENIS r_kmag_denis, B1mag_USNOB1 r_b1mag_usnob1, R1mag_USNOB1 r_r1mag_usnob1, B2mag_USNOB1 r_b2mag_usnob1, R2mag_USNOB1 r_r2mag_usnob1, Imag_USNOB1 r_imag_usnob1 FROM RAVEPUB_DR5.RAVE_DR2gaia_source G INNER JOIN RAVEPUB_DR5.RAVE_DR5 R ON R.RAVE_OBS_ID=G.RAVE_OBS_ID WHERE G.parallax IS NOT NULL

The following processing was done:

Removed rows with missing values in any of the following columns:             
"ra",
        "dec",
        "pmra",
        "pmdec",
        "l",
        "b",
        "parallax",
        "parallaxerror",
        "photgmeanmag",
        "photbpmeanmag",
        "photrpmeanmag",
        "rhrv",
        "rmetallicity",
        "rdistance",
        "rparallax",
        "rjmag2mass",
        "rhmag2mass",
        "rkmag2mass",
        "rmg",
        "rsi",
        "rfe",         
        "rquality",
        "rra",
        "rde",
        "rw1magallwise",
        "rw2magallwise",
        "rw3magallwise",
        "rw4magallwise",
        "rbmagapassdr9",
        "rvmagapassdr9",
        "rrpmagapassdr9",
        "ripmagapassdr9",
        "rimagdenis",
        "rjmagdenis",
        "rkmagdenis"
Averaged those values for rows that have the same Gaia "source_id".

Note
An alternative dataset is recommended: 257k Gaia DR2 stars. It contains sources from the Northern and Southern Hemispheres. 
Acknowledgements
This work has made use of data from the European Space Agency (ESA) mission Gaia (https://www.cosmos.esa.int/gaia), processed by the Gaia Data Processing and Analysis Consortium (DPAC, https://www.cosmos.esa.int/web/gaia/dpac/consortium). Funding for the DPAC has been provided by national institutions, in particular the institutions participating in the Gaia Multilateral Agreement.
References
Kunder et al. (2016). The Radial Velocity Experiment (RAVE): Fifth Data Release. arXiv:1609.03210
Zackrisson et al. (2018). SETI with Gaia: The observational signatures of nearly complete Dyson spheres. arXiv:1804.08351

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43669) of an [OpenML dataset](https://www.openml.org/d/43669). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43669/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43669/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43669/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

